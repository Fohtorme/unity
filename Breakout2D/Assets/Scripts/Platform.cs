﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    
    // Platform speed
    public float platformSpeed = 2f;
    public float scenaryWidth = 10f;

    // Use this for initialization
    void Start () {

		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        // Manage keys actions
        keyEvents();
    }
    

    // Manage keys actions
    void keyEvents()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Move platform to left
            movePlatform(Vector3.left);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            // Move platform to right
            movePlatform(Vector3.right);
        }
    }

    // Move platform to determined direction
    void movePlatform(Vector3 direction)
    {
        // Define platform reference acording by direction
        float platformReference = (transform.localScale.x / 2) * direction.x;
        // Define scenary limit acording by direction
        float scenaryLimit = (scenaryWidth / 2) * direction.x;
        // Define speed acording by direction
        float speed = Time.deltaTime * platformSpeed * direction.x;

        // If platform is in map limit
        if ((transform.position.x + platformReference - scenaryLimit) * direction.x < 0f)
        {
            // Move platform
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            // If platform move beyond the limit
            if ((transform.position.x + platformReference - scenaryLimit) * direction.x > 0f)
            {
                // Move platform to map limit
                transform.position = new Vector3(scenaryLimit - platformReference, transform.position.y, transform.position.z);
            }
        }
    }

}
